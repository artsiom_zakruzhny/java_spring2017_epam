All nodes mapping described in config.properties.
Each node is run on a separate Tomcat server on different ports (described in file).
Every node can receive request on GET, PUT, DELETE. For example, request: http://localhost:8090/by.epamlab.rest/rest/id2, can use for all operations. And in this example node0 is interface of cluster.
For check status nodes on each server start independent thread that runs every 30 seconds.
For get status about all cluster, sent GET request for example: http://localhost:8090/by.epamlab.rest/status. 
Can send a request for all available node (used all available ports). If node in this port not exist, then message about fail is displayed.
For get status about one node, for example node1, sent request : http://localhost:8091/by.epamlab.rest/status/node1. Situation with ports the same as for cluster.
Max key length 5 characters.
Used data base - H2. In init method call method for create tables if they not exist. Server for H2 start when call this method.
Config.properties:
1. Numbering of nodes start with 0 (every following node +1, except if it is not a replica);
2. The name of the replica begins with the name of the node plus postfix replic (and number replic, same nodes start with 0)
Example: node0=uri;node0replic0=uri;node1=uri;node1replic0=uri;node1replic1=uri;
Error:node5=uri, node6=uri; (See first point).