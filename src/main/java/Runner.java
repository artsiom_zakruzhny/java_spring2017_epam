
import java.lang.reflect.InvocationTargetException;

import by.training.epamlab.beans.*;
import by.training.epamlab.caches.LFU;
import by.training.epamlab.caches.LRU;
import by.training.epamlab.factorys.ProxyFactory;
import by.training.epamlab.interfaces.Animal;
import by.training.epamlab.utilities.Serializer;

public class Runner {

	public static void main(String[] args) {
		// Cat cat2 = new Cat("split", 30);
		// Cat cat = new Cat("split", 20);
		// Dog dog = new Dog("mahoni", 13);
		// Serializer serializer = new Serializer();
		// try {
		// System.out.println(serializer.serialized(cat));
		// System.out.println(serializer.serialized(dog));
		// System.out.println(serializer.serialized(cat2));
		//
		// System.out.println(serializer.equalObjects(cat, cat2));
		//
		// System.out.println(serializer.deserialized(serializer.serialized(cat),
		// Cat.class));
		//
		// System.out.println(serializer.deserialized(serializer.serialized(cat2),
		// Cat.class));
		// } catch (NoSuchMethodException | SecurityException |
		// InstantiationException | IllegalAccessException
		// | InvocationTargetException | IllegalArgumentException e1) {
		// // throw new IllegalArgumentException();
		// e1.printStackTrace();
		// }
		// try {
		// Animal cat1 = (Animal) Factory.getInstanceOf(Cat.class);
		// Animal dog1 = (Animal) Factory.getInstanceOf(Dog.class);
		// cat1.setName("joker");
		// System.out.println(cat1.getName());
		// dog1.setName("split");
		// System.out.println(dog1.getName());
		// cat1.jump();
		// dog1.jump();
		// } catch (NoSuchMethodException | SecurityException |
		// IllegalArgumentException | InvocationTargetException
		// | InstantiationException | IllegalAccessException e) {
		// throw new RuntimeException();
		// }
		//
		// LRU<Integer, String> cacheLRU = new LRU<Integer, String>(3);
		// cacheLRU.put(1, "�����");
		// cacheLRU.put(2, "����");
		// cacheLRU.put(3, "�����");
		// cacheLRU.put(4, "����");
		// cacheLRU.put(5, "�����");
		// cacheLRU.put(6, "�����");
		// System.out.println(cacheLRU);

		LFU<Integer, String> cacheLFU = new LFU<Integer, String>(3);
		cacheLFU.put(1, "������");

		cacheLFU.get(1);
		cacheLFU.get(1);
		cacheLFU.get(1);

		cacheLFU.put(2, "second");

		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);
		cacheLFU.get(2);

		cacheLFU.put(3, "third");

		cacheLFU.get(3);
		cacheLFU.get(3);
		cacheLFU.get(3);
		cacheLFU.get(3);
		cacheLFU.get(3);
		cacheLFU.get(3);

		cacheLFU.put(4, "���������");

		cacheLFU.get(4);
		cacheLFU.get(4);
		cacheLFU.get(4);
		cacheLFU.get(4);
		cacheLFU.get(4);
		cacheLFU.get(4);
		cacheLFU.get(4);

		cacheLFU.put(5, "�����");
		cacheLFU.put(6, "������");

		System.out.println(cacheLFU);

	}
}
