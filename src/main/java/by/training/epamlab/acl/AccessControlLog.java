package by.training.epamlab.acl;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "acl")
public class AccessControlLog {

	private UUID id;
	private String ip;
	private Date date;
	private String method;
	private String oldData;
	private String data;
	private String node;

	public AccessControlLog() {
	}

	@Id
	@Column(name = "id")
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Basic
	@Column(name = "ip")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Basic
	@Column(name = "date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Basic
	@Column(name = "method")
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Basic
	@Column(name = "oldData")
	public String getOldData() {
		return oldData;
	}

	public void setOldData(String oldData) {
		this.oldData = oldData;
	}

	@Basic
	@Column(name = "dataJson")
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Basic
	@Column(name = "node")
	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	@Override
	public String toString() {
		return id + "; " + ip + "; " + date + "; " + method + "; " + oldData + "; " + data + "; " + node;
	}

}
