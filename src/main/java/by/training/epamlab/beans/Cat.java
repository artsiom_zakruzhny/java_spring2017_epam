package by.training.epamlab.beans;

import by.training.epamlab.annotations.Equal;
import by.training.epamlab.annotations.Proxy;
import by.training.epamlab.annotations.Serializable;
import by.training.epamlab.interfaces.Animal;

@Serializable
@Proxy(invocationHandler = "by.gsu.epamlab.utilities.Handler")
public class Cat implements Animal {

	@Serializable(alias = "catName")
	@Equal
	private String name;
	@Serializable
	@Equal
	private float weight;

	@Serializable(alias = "catDog")
	private Dog dog;

	public Cat() {
	}

	public Cat(String name, float weight) {
		super();
		this.name = name;
		this.weight = weight;
	}

	public Cat(String name, float weight, Dog dog) {
		super();
		this.name = name;
		this.weight = weight;
		this.dog = dog;
	}

	public Dog getDog() {
		return dog;
	}

	public void setDog(Dog dog) {
		this.dog = dog;
	}

	@Override
	public void setName(String name) {
		this.name = name;

	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public float getWeight() {
		return weight;
	}

	@Override
	public void jump() {
		System.out.println("jump - jump");
	}

	@Override
	public String toString() {
		return "name=" + name + "; weight=" + weight + "; dog=" + dog;
	}

}
