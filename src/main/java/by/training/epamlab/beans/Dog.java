package by.training.epamlab.beans;

import by.training.epamlab.annotations.Serializable;
import by.training.epamlab.interfaces.Animal;

@Serializable
public class Dog implements Animal {

	@Serializable(alias = "dogName")
	private String name;
	@Serializable(alias = "dogWeight")
	private float weight;

	@Serializable(alias = "dogCat")
	private Cat cat;

	public Dog() {
	}

	public Dog(String name, float weight) {
		super();
		this.name = name;
		this.weight = weight;
	}

	public Dog(String name, float weight, Cat cat) {
		super();
		this.name = name;
		this.weight = weight;
		this.cat = cat;
	}

	public Cat getCat() {
		return cat;
	}

	public void setCat(Cat cat) {
		this.cat = cat;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public float getWeight() {
		return weight;
	}

	@Override
	public void jump() {
		System.out.println("jump");
	}

	@Override
	public String toString() {
		return "name=" + name + "; weight=" + weight + "; cat=" + cat;
	}

}
