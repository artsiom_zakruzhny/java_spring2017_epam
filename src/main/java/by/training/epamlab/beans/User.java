package by.training.epamlab.beans;

import by.training.epamlab.annotations.Serializable;

@Serializable
public class User {

	@Serializable
	private String name;
	@Serializable
	private String secondName;
	@Serializable
	private int age;

	public User() {

	}

	public User(String name, String secondName, int age) {
		super();
		this.name = name;
		this.secondName = secondName;
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Override
	public String toString() {
		return name + "; " + secondName + "; " + age;
	}

}
