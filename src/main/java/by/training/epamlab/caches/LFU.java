package by.training.epamlab.caches;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import by.training.epamlab.interfaces.Cache;

public class LFU<K, V> implements Cache<K, V> {
	private final int initialCapacity;
	private final Map<K, Node> map;

	public LFU(int initialCapacity) {
		if (initialCapacity <= 0) {
			throw new IllegalArgumentException();
		}
		this.initialCapacity = initialCapacity;
		this.map = new LinkedHashMap<>(initialCapacity, 1);
	}

	@Override
	public synchronized V put(K key, V value) {
		eviction();
		Node oldNode = map.put(key, new Node(value));
		if (oldNode == null) {
			return null;
		}
		return oldNode.getValue();
	}

	private void eviction() {
		if (map.size() < initialCapacity) {
			return;
		}
		map.remove(getKeyWithMinFrequency());
	}

	@Override
	public synchronized V get(K key) {
		Node node = map.get(key);
		if (node == null) {
			return null;
		}
		node.incrementFrequency();
		return node.getValue();
	}

	@Override
	public synchronized V delete(K key) {
		if (map.containsKey(key)) {
			return map.remove(key).getValue();
		}
		return null;
	}

	private K getKeyWithMinFrequency() {
		long minFreq = Long.MAX_VALUE;
		K key = null;
		Iterator<Map.Entry<K, Node>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<K, Node> node = iterator.next();
			if (minFreq > node.getValue().getFrequency()) {
				key = node.getKey();
				minFreq = node.getValue().getFrequency();
			}
		}
		return key;
	}

	@Override
	public String toString() {
		return map + "";
	}

	public class Node {
		private final V value;
		private int frequency;

		public Node(V value) {
			this.value = value;
			this.frequency = 1;
		}

		public int getFrequency() {
			return frequency;
		}

		public V getValue() {
			return value;
		}

		public int incrementFrequency() {
			return ++frequency;
		}

		@Override
		public String toString() {
			return value + "; " + frequency;
		}

	}

}
