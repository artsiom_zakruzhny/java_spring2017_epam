package by.training.epamlab.caches;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import by.training.epamlab.interfaces.Cache;

public class LRU<K, V> implements Cache<K, V> {
	private final LRUStorage storage;

	private final static boolean ACCESS_ORDER = true;
	private final static float LOAD_FACTOR = 0.75F;

	@Override
	public synchronized V put(K key, V value) {
		return storage.put(key, value);
	}

	@Override
	public synchronized V get(K key) {
		return storage.get(key);
	}

	@Override
	public synchronized V delete(K key) {
		if (storage.containsKey(key)) {
			return storage.remove(key);
		}
		return null;
	}

	public LRU(int initialCapacity) {
		if (initialCapacity <= 0) {
			throw new IllegalArgumentException("Capacity should be more 0.");
		}
		storage = new LRUStorage(initialCapacity, LOAD_FACTOR, ACCESS_ORDER);
	}

	private class LRUStorage extends LinkedHashMap<K, V> {
		private static final long serialVersionUID = 1L;
		private final int initialCapacity;

		public LRUStorage(int initialCapacity, float loadFactor, boolean accessOrder) {
			super(initialCapacity, loadFactor, accessOrder);
			this.initialCapacity = initialCapacity;
		}

		@Override
		protected boolean removeEldestEntry(Entry<K, V> eldest) {
			return size() > this.initialCapacity;
		}

	}

	@Override
	public String toString() {
		return storage + "";
	}

}
