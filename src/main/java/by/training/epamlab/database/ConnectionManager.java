package by.training.epamlab.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

public class ConnectionManager {
	private final static String CLASS_NAME = "org.h2.Driver";
	private final static String HOST = "jdbc:h2:tcp://localhost/~/rest;DB_CLOSE_ON_EXIT=FALSE";
	private final static String SERVERSETTINGS = "-tcpAllowOthers";
	private final static String USER_NAME = "root";
	private final static String PASSWORD = "13061977Mh";

	public static Connection getConnection() throws SQLException {
		Connection connection = null;
		try {
			Server.createTcpServer(SERVERSETTINGS).start();
			Class.forName(CLASS_NAME);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Data base driver not found.");
		}
		connection = DriverManager.getConnection(HOST, USER_NAME, PASSWORD);
		return connection;
	}

	public static void closeResultSet(ResultSet... rs) {
		for (ResultSet resultSet : rs) {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new IllegalArgumentException("Exception on closing ResultSet.");
				}
			}
		}
	}

	public static void closeStatement(Statement... statement) {
		for (Statement st : statement)
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					throw new IllegalArgumentException("Exception on closing Statements.");
				}
			}
	}

	public static void closeConnection(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new IllegalArgumentException("Exception on closing Connection.");
			}
		}
	}
}
