package by.training.epamlab.factorys;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

public class ProxyFactory {

	public static Object getInstanceOf(Class<?> clazz) throws InstantiationException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException, IllegalAccessException {
		Object obj = clazz.newInstance();
		if (clazz.isAnnotationPresent(by.training.epamlab.annotations.Proxy.class)) {
			String invocationHandlerClass = clazz.getAnnotation(by.training.epamlab.annotations.Proxy.class)
					.invocationHandler();
			Class<?> handler = null;
			try {
				handler = Class.forName(invocationHandlerClass);
			} catch (ClassNotFoundException e) {
				throw new IllegalArgumentException();
			}
			Constructor<?> con = handler.getConstructor(Object.class);
			return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
					(InvocationHandler) con.newInstance(obj));
		}
		return obj;
	}
}
