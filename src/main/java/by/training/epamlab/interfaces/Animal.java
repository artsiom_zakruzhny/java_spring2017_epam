package by.training.epamlab.interfaces;

public interface Animal {
	void setName(String name);

	String getName();

	void setWeight(float weight);

	float getWeight();

	void jump();
}
