package by.training.epamlab.interfaces;

public interface Cache<K, V> {
	V put(K key, V value);

	V get(K key);

	V delete(K key);

}
