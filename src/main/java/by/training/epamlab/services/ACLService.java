package by.training.epamlab.services;

import java.util.Calendar;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import by.training.epamlab.acl.AccessControlLog;
import by.training.epamlab.factorys.HibernateSessionFactory;

public class ACLService {
	private Session session = HibernateSessionFactory.getSessionFactory().openSession();

	public void log(HttpServletRequest request, String oldDate, String data, String node) {
		try {
			session.beginTransaction();
			AccessControlLog accessControlList = new AccessControlLog();
			accessControlList.setId(UUID.randomUUID());
			accessControlList.setDate(Calendar.getInstance().getTime());
			accessControlList.setIp(request.getRemoteAddr());
			accessControlList.setMethod(request.getMethod());
			accessControlList.setOldData(oldDate);
			accessControlList.setData(data);
			accessControlList.setNode(node);
			session.save(accessControlList);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
		} finally {
			HibernateSessionFactory.closeSession(session);
		}
	}
}
