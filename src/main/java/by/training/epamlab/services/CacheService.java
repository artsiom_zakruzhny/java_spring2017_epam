package by.training.epamlab.services;

import org.apache.log4j.Logger;

import by.training.epamlab.caches.LFU;
import by.training.epamlab.caches.LRU;
import by.training.epamlab.interfaces.Cache;

public class CacheService {
	private static final Logger LOG = Logger.getLogger(CacheService.class);
	private static Cache<String, String> cache;
	private DataBaseService dataBaseService;

	public static void setCache(int algorithm, int capacity) {
		switch (algorithm) {
		case 0: {
			cache = new LFU<>(capacity);
			break;
		}
		case 1: {
			cache = new LRU<>(capacity);
			break;
		}
		default:
			throw new IllegalArgumentException("Not valid algorithm.");
		}
	}

	public static Cache<String, String> getCache() {
		return cache;
	}

	public void setDataBaseService(DataBaseService dataBaseService) {
		this.dataBaseService = dataBaseService;
	}

	public DataBaseService getDataBaseService() {
		return dataBaseService;
	}

	public int getNumberNode(final int hash, final int amountNodes) {
		return hash % amountNodes;
	}

	public void printCache() {
		System.out.println(cache);
	}

	public String getCacheValue(String node, String key) {
		String getFromCache = null;
		LOG.info("Get value from cache.");
		getFromCache = cache.get(key);
		System.out.println("from cache:" + getFromCache);
		if (getFromCache == null) {
			return dataBaseService.selectById(node, key);
		}
		LOG.info("No value associated with key for get request.");
		return getFromCache;
	}

	public String putValue(String node, String key, String object) {
		String oldData = null;
		oldData = dataBaseService.insertData(node, key, object);
		LOG.info("Put value in cache.");
		String oldLfuData = cache.put(key, object);
		if (oldLfuData != null) {
			return oldLfuData;
		}
		LOG.info("No valid request for put.");
		return oldData;
	}

	public String deleteValue(String node, String key) {
		String deleteValueFromCache = null;
		String deleteValueFromDB = null;
		deleteValueFromDB = dataBaseService.deleteData(node, key);
		if (deleteValueFromDB == null) {
			LOG.info("No value associated  with key for delete.");
			return deleteValueFromDB;
		}
		LOG.info("Delete value from cache.");
		deleteValueFromCache = cache.delete(key);
		if (deleteValueFromCache != null) {
			return deleteValueFromCache;
		}
		return deleteValueFromDB;
	}
}
