package by.training.epamlab.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import by.training.epamlab.database.ConnectionManager;

public class DataBaseService {
	private static final Logger LOG = Logger.getLogger(DataBaseService.class);
	private Map<String, String> selectNodeReplic = new HashMap<>();

	public Map<String, String> getSelectNodeReplic() {
		return selectNodeReplic;
	}

	public static void createTable(String tableName) {
		LOG.info("Start Create tables.");
		Connection connection = null;
		Statement statement = null;
		String query = null;
		try {
			connection = ConnectionManager.getConnection();
			statement = connection.createStatement();
			query = "CREATE TABLE if not exists " + tableName
					+ " (keyNode VARCHAR(45) NOT NULL, valueNode VARCHAR(255), PRIMARY KEY (keyNode));";
			statement.execute(query);
		} catch (SQLException e) {
			LOG.error("sql exception while create tables.");
			e.printStackTrace();
			throw new IllegalArgumentException("Exception while creating tables.");
		} finally {
			ConnectionManager.closeStatement(statement);
			ConnectionManager.closeConnection(connection);
		}
		LOG.info("Finish Create tables.");
	}

	public String selectById(String nameNode, String id) {
		LOG.info("Start work with database for select by id.");
		final String QUERY = "SELECT valueNode FROM " + nameNode + " WHERE keyNode = (?);";
		Connection connection = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		try {
			connection = ConnectionManager.getConnection();
			psSelect = connection.prepareStatement(QUERY);
			psSelect.setString(1, id);
			rs = psSelect.executeQuery();
			if (rs.next()) {
				LOG.info("Get data from database if not in cache.");
				return rs.getString(1);
			}
		} catch (SQLException e) {
			LOG.error("sql exception.");
			e.printStackTrace();
			throw new IllegalArgumentException("sql exception while select data...");
		} finally {
			LOG.info("Finish work with database for select.");
			ConnectionManager.closeResultSet(rs);
			ConnectionManager.closeStatement(psSelect);
			ConnectionManager.closeConnection(connection);
		}
		LOG.info("No value in database.");
		return null;
	}

	public void selectAllData(String nameNode) {
		LOG.info("Start work with database for select all data for syn�.");
		final String QUERY = "SELECT keyNode, valueNode FROM " + nameNode;
		Connection connection = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		try {
			connection = ConnectionManager.getConnection();
			psSelect = connection.prepareStatement(QUERY);
			rs = psSelect.executeQuery();
			while (rs.next()) {
				selectNodeReplic.put(rs.getString(1), rs.getString(2));
				LOG.info("Put data in storage for sync.");
			}
		} catch (SQLException e) {
			LOG.error("sql exception.");
			e.printStackTrace();
			throw new IllegalArgumentException("sql exception while select data...");
		} finally {
			LOG.info("Finish work with database for select.");
			ConnectionManager.closeResultSet(rs);
			ConnectionManager.closeStatement(psSelect);
			ConnectionManager.closeConnection(connection);
		}
	}

	public String insertData(String nameNode, String key, String jsonStr) {
		LOG.info("Start work with database for insert.");
		final String QUERY = "INSERT INTO " + nameNode + " (keyNode, valueNode) VALUES (?,?);";
		Connection connection = null;
		PreparedStatement psInsert = null;
		try {
			String oldData = selectById(nameNode, key);
			if (oldData != null) {
				LOG.info("Key is exist.");
				updateData(nameNode, key, jsonStr);
				return oldData;
			} else {
				connection = ConnectionManager.getConnection();
				psInsert = connection.prepareStatement(QUERY);
				psInsert.setString(1, key);
				psInsert.setString(2, jsonStr);
				psInsert.executeUpdate();
				LOG.info("Data inserted in database.");
			}
			return jsonStr;
		} catch (SQLException e) {
			LOG.error("Data not inserted in database.");
			e.printStackTrace();
			throw new IllegalArgumentException("sql exception while insert data...");
		} finally {
			LOG.info("Finish work with database for insert.");
			ConnectionManager.closeStatement(psInsert);
			ConnectionManager.closeConnection(connection);
		}
	}

	private boolean updateData(String nameNode, String key, String jsonStr) {
		LOG.info("Start work with database for update.");
		final String QUERY = "UPDATE " + nameNode + " SET valueNode = ? Where keyNode = ?;";
		Connection connection = null;
		PreparedStatement psUpdate = null;
		try {
			connection = ConnectionManager.getConnection();
			psUpdate = connection.prepareStatement(QUERY);
			psUpdate.setString(1, jsonStr);
			psUpdate.setString(2, key);
			psUpdate.executeUpdate();
			LOG.info("Data updated in database.");
			return true;
		} catch (SQLException e) {
			LOG.error("Data not updated in database.");
			e.printStackTrace();
			throw new IllegalArgumentException("sql exception while update data...");
		} finally {
			LOG.info("Finish work with database for update.");
			ConnectionManager.closeStatement(psUpdate);
			ConnectionManager.closeConnection(connection);
		}
	}

	public String deleteData(String nameNode, String key) {
		LOG.info("Start work with database for delete.");
		final String sqlQuery = "DELETE FROM " + nameNode + " WHERE keyNode=(?);";
		Connection connection = null;
		PreparedStatement psDelete = null;
		try {
			String result = selectById(nameNode, key);
			if (result == null) {
				LOG.info("No value for delete associated with key.");
				return null;
			}
			connection = ConnectionManager.getConnection();
			psDelete = connection.prepareStatement(sqlQuery);
			psDelete.setString(1, key);
			psDelete.executeUpdate();
			LOG.info("Deleted value.");
			return result;
		} catch (SQLException e) {
			LOG.error("sql exception while deleting.");
			throw new RuntimeException("sql exception while delete data...");
		} finally {
			LOG.info("Finish work with database for delete.");
			ConnectionManager.closeStatement(psDelete);
			ConnectionManager.closeConnection(connection);
		}
	}
}
