package by.training.epamlab.services;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWorkerService {
	public final static String CSVFILENAME = "src\\main\\resources\\start.csv";

	public static String readCSVFile(String fileName) {
		int start = 0;
		try (FileReader reader = new FileReader(fileName)) {
			start = reader.read();
		} catch (IOException e) {
			throw new IllegalArgumentException("Exception while read csv file...");
		}
		return String.valueOf(start);
	}

	public static void writeCSVFile(String fileName, int start) {
		try (FileWriter writer = new FileWriter(fileName)) {
			writer.write(start);
			writer.flush();
		} catch (IOException e) {
			throw new IllegalArgumentException("exception while write in file");
		}
	}

}
