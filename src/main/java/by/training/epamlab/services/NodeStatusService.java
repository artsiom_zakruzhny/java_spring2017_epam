package by.training.epamlab.services;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

public class NodeStatusService {
	private static final String INFO_IS_NULL = "Information is not collected";
	private static final Logger LOG = Logger.getLogger(NodeStatusService.class);
	private static Map<String, String> statusNodesWithReplics = new HashMap<>();
	private static Map<String, String> statusNodes = new HashMap<>();

	public void startCheckStatusThred(final ServletContext context, final Map<String, Map<String, String>> replics) {
		final int FIRST_START = 1;
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				LOG.info("Start thread.");
				boolean checkRightNode = false;
				for (Map.Entry<String, Map<String, String>> entry : replics.entrySet()) {
					Map<String, String> map = entry.getValue();
					checkRightNode = true;
					for (Map.Entry<String, String> replic : map.entrySet()) {
						int code = RequestorService.sendCheckNodes(replic.getKey(), replic.getValue());
						if (code == 200) {
							statusNodesWithReplics.put(replic.getKey(), "OK");
						} else {
							statusNodesWithReplics.put(replic.getKey(), "crash");
							checkRightNode = false;
						}
					}
					if (checkRightNode) {
						statusNodes.put(entry.getKey(), "OK");
					} else {
						statusNodes.put(entry.getKey(), "crach");
					}
				}
				context.setAttribute("status", statusNodesWithReplics);
				context.setAttribute("clusterStatus", statusNodes);
				LOG.info("checked all entrys");
			}
		}, 0, 5, TimeUnit.SECONDS);
		FileWorkerService.writeCSVFile(FileWorkerService.CSVFILENAME, FIRST_START);
	}

	public static boolean checkClusterStatus() {
		for (Map.Entry<String, String> entry : statusNodes.entrySet()) {
			if (entry.getValue().equals("crach")) {
				System.out.println("wait");
				return false;
			}
		}
		return true;
	}

	public String getNodeStatus(String node) {
		String status = null;
		if (statusNodesWithReplics == null) {
			LOG.info(INFO_IS_NULL);
			return INFO_IS_NULL;
		}
		if (node == null) {
			status = statusNodesWithReplics.toString();
			LOG.info("get status all cluster: " + status);
		} else {
			String nodeId = node.substring(1);
			LOG.info("get status single node: " + nodeId);
			if (statusNodesWithReplics.containsKey(nodeId)) {
				status = statusNodesWithReplics.get(nodeId);
			}
		}
		return status;
	}

	public static Map<String, String> getStatusNodesWithReplics() {
		return statusNodesWithReplics;
	}

	public static void setStatusNodesWithReplics(Map<String, String> statusNodesWithReplics) {
		NodeStatusService.statusNodesWithReplics = statusNodesWithReplics;
	}

	public static Map<String, String> getStatusNodes() {
		return statusNodes;
	}

	public static void setStatusNodes(Map<String, String> statusNodes) {
		NodeStatusService.statusNodes = statusNodes;
	}

}
