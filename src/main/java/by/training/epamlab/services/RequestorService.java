package by.training.epamlab.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

public class RequestorService {
	private static final Logger LOG = Logger.getLogger(RequestorService.class);
	public static final String REDIRECT = "?redirect=1";

	public static int sendCheckNodes(String node, String url) {
		LOG.info("Start check " + node);
		int code = 0;
		HttpClient client = HttpClientBuilder.create().build();
		HttpHead request = new HttpHead(url);
		try {
			LOG.info("Get status code " + node);
			HttpResponse response = client.execute(request);
			code = response.getStatusLine().getStatusCode();
			LOG.info(node + " works good on url: " + url);
		} catch (IOException e) {
			LOG.error(node + "don't work on url: " + url);
			code = 500;
		}
		LOG.info("Finich check " + node);
		return code;
	}

	public static String redirectGet(String id, Map<String, String> mapping) {
		String result = null;
		Iterator<Entry<String, String>> iterator = mapping.entrySet().iterator();
		while (result == null && iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			String name = entry.getKey();
			String url = MessageFormat.format(entry.getValue() + "{0}{1}", id, REDIRECT);
			System.out.println("get from node: " + name + "; uri: " + url);
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);
			request.setHeader("nodeName", name);
			LOG.info("Get info from: " + name);
			try {
				HttpResponse response = client.execute(request);
				result = readResponseInputStream(response.getEntity().getContent());
			} catch (IOException e) {
				LOG.error("Get fail from node: " + name);
				result = name + " don't work on url " + url;
			}
		}
		return result;
	}

	public static String redirectPut(String id, String dataJson, Map<String, String> mapping) {
		String result = null;
		HttpClient client = HttpClientBuilder.create().build();
		Map<String, String> addedReplics = new HashMap<>();
		for (Map.Entry<String, String> entry : mapping.entrySet()) {
			String name = entry.getKey();
			String url = MessageFormat.format(entry.getValue() + "{0}{1}", id, REDIRECT);
			LOG.info("Put info in: " + name);
			System.out.println("put in node: " + name + "; uri: " + url);
			HttpPut request = new HttpPut(url);
			StringEntity data = new StringEntity(dataJson, StandardCharsets.UTF_8);
			request.setHeader("nodeName", name);
			request.setEntity(data);
			try {
				HttpResponse response = client.execute(request);
				result = readResponseInputStream(response.getEntity().getContent());
				addedReplics.put(name, entry.getValue());
			} catch (IOException e) {
				LOG.error("Put fail in node: " + name);
				System.err.println(name + " don't work on url " + url);
				System.out.println("replics fot rollback:" + addedReplics);
				redirectDelete(id, addedReplics); // rollback
			}
		}
		return result;
	}

	public static String redirectDelete(String id, Map<String, String> mapping) {
		String result = null;
		HttpClient client = HttpClientBuilder.create().build();
		Map<String, String> deletedReplics = new HashMap<>();
		for (Map.Entry<String, String> entry : mapping.entrySet()) {
			String name = entry.getKey();
			String url = MessageFormat.format(entry.getValue() + "{0}{1}", id, REDIRECT);
			LOG.info("Delete info from: " + name);
			System.out.println("delete from node: " + name + "; uri: " + url);
			HttpDelete request = new HttpDelete(url);
			request.setHeader("nodeName", name);
			try {
				HttpResponse response = client.execute(request);
				result = readResponseInputStream(response.getEntity().getContent());
				deletedReplics.put(name, entry.getValue());
			} catch (IOException e) {
				LOG.error("Delete fail from node: " + name);
				System.err.println(name + " don't work on url " + url);
				redirectPut(id, result, deletedReplics); // rollback
			}
		}
		return result;
	}

	private static String readResponseInputStream(InputStream inputStream) {
		Scanner scanner = null;
		String result = null;
		try {
			scanner = new Scanner(inputStream);
			StringBuilder builder = new StringBuilder();
			while (scanner.hasNext()) {
				builder.append(scanner.nextLine());
			}
			result = builder.toString();
		} finally {
			scanner.close();
		}
		return result;
	}

}
