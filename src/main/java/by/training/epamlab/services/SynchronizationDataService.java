package by.training.epamlab.services;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class SynchronizationDataService {
	private static final Logger LOG = Logger.getLogger(SynchronizationDataService.class);

	public static void waitCluster(Map<String, Map<String, String>> replics) {
		final int NOT_FIRST_START = 0;
		int start = Integer.parseInt(FileWorkerService.readCSVFile(FileWorkerService.CSVFILENAME));
		System.out.println("start = " + start);
		if (start == 1) {
			LOG.info("Wait until the cluster is loaded.");
			while (!NodeStatusService.checkClusterStatus()) {
				try {
					LOG.info("Wait 30 sec and check again.");
					TimeUnit.SECONDS.sleep(30);
				} catch (InterruptedException e) {
					LOG.error("Interrupted exception while loading cluster.");
					throw new RuntimeException("Interrupted exception while loading cluster.");
				}
			}
			SynchronizationDataService.synchronizeReplicsData(replics);
			SynchronizationDataService.synchronizeNodeData(replics);
		}
		FileWorkerService.writeCSVFile(FileWorkerService.CSVFILENAME, NOT_FIRST_START);
		LOG.info("�luster loaded.");
	}

	private static void synchronizeReplicsData(Map<String, Map<String, String>> replics) {
		LOG.info("Synchronize replics.");
		for (Map.Entry<String, Map<String, String>> node : replics.entrySet()) {
			DataBaseService dataBaseService = new DataBaseService();
			getDataForSync(dataBaseService, node);
			distributeData(dataBaseService, node);
		}
	}

	/*
	 * public static void synchronizeNodeDataWithCache(Map<String, Map<String,
	 * String>> replics) { DataBaseService dataBaseService = new
	 * DataBaseService(); CacheService service = new CacheService(); for
	 * (Map.Entry<String, Map<String, String>> node : replics.entrySet()) {
	 * String oldNodeName = node.getKey(); System.out.println("NODE:" +
	 * oldNodeName); Map<String, String> nodeValue = node.getValue(); for
	 * (Map.Entry<String, String> entry : nodeValue.entrySet()) {
	 * dataBaseService.selectData(entry.getKey(), null); for (Map.Entry<String,
	 * String> entrySync : dataBaseService.getSelectNodeReplic().entrySet()) {
	 * String id = entrySync.getKey(); String dataJson =
	 * dataBaseService.selectData(entry.getKey(), entrySync.getKey());
	 * System.out.println("id = " + entrySync.getKey()); int numberNode =
	 * service.getNumberNode(entrySync.getKey().hashCode(), replics.size());
	 * String newNodeName = "node" + numberNode; System.out.println("new Node: "
	 * + newNodeName); Map<String, String> mappingPut =
	 * replics.get(newNodeName); Map<String, String> mappingDelete =
	 * replics.get(oldNodeName); if (!newNodeName.equals(oldNodeName)) {
	 * System.out.println("����������."); RequestorService.redirectDelete(id,
	 * mappingDelete, "?redirect=1"); RequestorService.redirectPut(id, dataJson,
	 * mappingPut, "?redirect=1"); } } } } }
	 */

	private static void synchronizeNodeData(Map<String, Map<String, String>> replics) {
		LOG.info("Synchronize nodes:");
		CacheService service = new CacheService();
		for (Map.Entry<String, Map<String, String>> node : replics.entrySet()) {
			DataBaseService dataBaseService = new DataBaseService();
			String oldNodeName = node.getKey();
			System.out.println("NODE:" + oldNodeName);
			Iterator<Entry<String, String>> iteratorReplics = node.getValue().entrySet().iterator();
			if (iteratorReplics.hasNext()) {
				String tableName = iteratorReplics.next().getKey();
				dataBaseService.selectAllData(tableName);
				for (Map.Entry<String, String> entrySync : dataBaseService.getSelectNodeReplic().entrySet()) {
					String id = entrySync.getKey();
					String dataJson = dataBaseService.selectById(tableName, entrySync.getKey());
					System.out.println("id = " + id);
					int numberNode = service.getNumberNode(entrySync.getKey().hashCode(), replics.size());
					String newNodeName = "node" + numberNode;
					LOG.info("Defined new storage node:" + newNodeName);
					System.out.println("new Node: " + newNodeName);
					Map<String, String> mappingPut = replics.get(newNodeName);
					Map<String, String> mappingDelete = replics.get(oldNodeName);
					if (replics.size() > 1 && (!newNodeName.equals(oldNodeName))) {
						LOG.info("Rewrite data in nodes:");
						System.out.println("����������.");
						deleteFromNode(dataBaseService, mappingDelete, id);
						insertIntoNode(dataBaseService, mappingPut, id, dataJson);
					}
				}
			}
		}
	}

	private static void insertIntoNode(DataBaseService service, Map<String, String> mapping, String id, String data) {
		for (String nodeName : mapping.keySet()) {
			LOG.info("Insert into:" + nodeName + " key = " + id + "; data = " + data);
			System.out.println("��������� � " + nodeName + " ���� " + id + " � �������: " + data);
			service.insertData(nodeName, id, data);
		}
	}

	private static void deleteFromNode(DataBaseService service, Map<String, String> mapping, String id) {
		for (String nodeName : mapping.keySet()) {
			LOG.info("Delete from:" + nodeName + " key = " + id);
			System.out.println("������� �� " + nodeName + " ���� " + id);
			service.deleteData(nodeName, id);
		}
	}

	private static void getDataForSync(DataBaseService dataBaseService, Entry<String, Map<String, String>> node) {
		LOG.info("Get data for sync replics:");
		Iterator<Entry<String, String>> selectedNodeValue = node.getValue().entrySet().iterator();
		while (selectedNodeValue.hasNext()) {
			Entry<String, String> nodeReplic = selectedNodeValue.next();
			LOG.info("Get data for sync replics from " + nodeReplic.getKey());
			dataBaseService.selectAllData(nodeReplic.getKey());
		}
	}

	private static void distributeData(DataBaseService dataBaseService, Entry<String, Map<String, String>> node) {
		LOG.info("Distribute data for sync replics:");
		Iterator<Entry<String, String>> iterator = node.getValue().entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> nodeReplic = iterator.next();
			for (Map.Entry<String, String> entry : dataBaseService.getSelectNodeReplic().entrySet()) {
				dataBaseService.insertData(nodeReplic.getKey(), entry.getKey(), entry.getValue());
				LOG.info("Insert data in" + nodeReplic.getKey() + " for sync replics: key = " + entry.getKey()
						+ " data = " + entry.getValue());
			}
		}
	}
}
