package by.training.epamlab.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.training.epamlab.services.NodeStatusService;

public class CheckStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(CheckStatusServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.info("Request for get info about status nodes come.");
		String node = request.getPathInfo();
		NodeStatusService statusService = new NodeStatusService();
		String status = statusService.getNodeStatus(node);
		PrintWriter out = response.getWriter();
		out.println(status);
		LOG.info("Info about status nodes printed.");
	}
}
