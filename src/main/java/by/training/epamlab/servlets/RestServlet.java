package by.training.epamlab.servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import by.training.epamlab.services.ACLService;
import by.training.epamlab.services.CacheService;
import by.training.epamlab.services.DataBaseService;
import by.training.epamlab.services.NodeStatusService;
import by.training.epamlab.services.RequestorService;
import by.training.epamlab.services.SynchronizationDataService;

public class RestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(RestServlet.class);
	private static final String NODE = "node";
	private final Map<String, String> mapping = new HashMap<>();
	private final Map<String, Map<String, String>> replics = new HashMap<>();
	private int amountNodes;

	@Override
	public void init() throws ServletException {
		LOG.info("Read config file.");
		ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
		Enumeration<String> keys = resourceBundle.getKeys();
		int cacheCapacity = 0;
		int algorithm = 0;
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			switch (key) {
			case "cacheAlgorithm": {
				algorithm = Integer.valueOf(resourceBundle.getString(key).trim());
				LOG.info("Received alghorithm: " + algorithm);
				break;
			}
			case "cacheCapacity": {
				cacheCapacity = Integer.valueOf(resourceBundle.getString(key).trim());
				LOG.info("Received cache capacity: " + cacheCapacity);
				break;
			}
			default: {
				Map<String, String> map = replics.get(key.substring(0, 5));
				if (map == null) {
					replics.put(key.substring(0, 5), map = new HashMap<>());
				}
				String uri = resourceBundle.getString(key);
				map.put(key, uri);
				mapping.put(key, uri);
				DataBaseService.createTable(key);
				break;
			}
			}
		}
		CacheService.setCache(algorithm, cacheCapacity);
		amountNodes = replics.size();
		LOG.info("Readed data: amount: " + amountNodes + "; algorithm = " + algorithm + "; capacity:" + cacheCapacity);
		NodeStatusService checkService = new NodeStatusService();
		checkService.startCheckStatusThred(getServletContext(), replics);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.info("GET request come.");
		setHeaders(response);
		SynchronizationDataService.waitCluster(replics);
		String resultGson = null;
		CacheService service = new CacheService();
		String id = request.getPathInfo().substring(1);
		int numberNode = service.getNumberNode(id.hashCode(), amountNodes);
		String node = NODE + numberNode;
		if (request.getParameter("redirect") != null) {
			LOG.info("GET request redirected.");
			resultGson = getDataAndWriteACL(request, service, id);
			LOG.info("GET is complete.");
		} else {
			LOG.info("GET request redirect.");
			Map<String, String> mapping = (replics.containsKey(node) ? replics.get(node)
					: replics.values().iterator().next());
			resultGson = RequestorService.redirectGet(id, mapping);
		}
		printResponse(response, resultGson);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.info("PUT request come.");
		String result = null;
		setHeaders(response);
		SynchronizationDataService.waitCluster(replics);
		CacheService service = new CacheService();
		String id = request.getPathInfo().substring(1);
		int numberNode = service.getNumberNode(id.hashCode(), amountNodes);
		String node = NODE + numberNode;
		String dataJson = readRequestBody(request);
		if (request.getParameter("redirect") != null) {
			result = putDataAndWriteACL(request, service, id, dataJson);
			service.printCache();
			LOG.info("PUT is complete.");
		} else {
			if (!checkNode(node)) {
				LOG.info("Fail. Can't put.");
				result = "{   \"fail\": \"Can't put\"}";
			} else {
				Map<String, String> mapping = (replics.containsKey(node) ? replics.get(node)
						: replics.values().iterator().next());
				result = RequestorService.redirectPut(id, dataJson, mapping);
			}
		}
		printResponse(response, result);
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.info("DELETE request come.");
		String result = null;
		setHeaders(response);
		SynchronizationDataService.waitCluster(replics);
		CacheService service = new CacheService();
		String id = request.getPathInfo().substring(1);
		int numberNode = service.getNumberNode(id.hashCode(), amountNodes);
		String node = NODE + numberNode;
		if (request.getParameter("redirect") != null) {
			result = deleteDataAndWriteACL(request, service, id);
			LOG.info("DELETE is complete.");
		} else {
			if (!checkNode(node)) {
				LOG.info("Fail. Can't delete.");
				result = "{   \"fail\": \"Can't delete\"}";
			} else {
				Map<String, String> mapping = (replics.containsKey(node) ? replics.get(node)
						: replics.values().iterator().next());
				result = RequestorService.redirectDelete(id, mapping);
			}
		}
		printResponse(response, result);
	}

	@Override
	protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOG.info("HEAD request come for check nodes.");
		resp.setStatus(200);
		LOG.info("HEAD is complete.");
	}

	/*
	 * @Override public void destroy() { try {
	 * Server.createTcpServer("-tcpAllowOthers").stop(); } catch (SQLException
	 * e) { throw new
	 * IllegalArgumentException("sql exception while stop server."); } }
	 */

	private boolean checkNode(String node) {
		@SuppressWarnings("unchecked")
		Map<String, String> statusNodes = (Map<String, String>) getServletContext().getAttribute("clusterStatus");
		if (statusNodes.containsKey(node)) {
			if (statusNodes.get(node).equals("crach")) {
				return false;
			}
		}
		return true;
	}

	private String readRequestBody(HttpServletRequest request) throws IOException {
		StringBuffer stringBuffer = new StringBuffer();
		String str = null;
		while ((str = request.getReader().readLine()) != null) {
			LOG.info("Read request body.");
			stringBuffer.append(str);
		}
		return stringBuffer.toString();
	}

	private void printResponse(HttpServletResponse response, String json) {
		try {
			response.getWriter().print(json);
		} catch (IOException e) {
			throw new IllegalArgumentException();
		}
	}

	private String dataBaseServiceHelper(HttpServletRequest req, CacheService service) {
		String nameNode = req.getHeader("nodeName");
		DataBaseService dataBaseService = new DataBaseService();
		service.setDataBaseService(dataBaseService);
		return nameNode;
	}

	private void setHeaders(HttpServletResponse response) {
		LOG.info("Set Headers.");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/JSON");
	}

	private void writeACLLog(HttpServletRequest req, String oldDate, String data, String node) {
		LOG.info("Write ACL LOG.");
		ACLService aclService = new ACLService();
		aclService.log(req, oldDate, data, node);
	}

	private String deleteDataAndWriteACL(HttpServletRequest req, CacheService service, String id) {
		String node = dataBaseServiceHelper(req, service);
		Gson gson = new Gson();
		String resultGson = gson.toJson(service.deleteValue(node, id));
		writeACLLog(req, null, resultGson, node);
		LOG.info("Deleted data:" + resultGson + " from node: " + node);
		return resultGson;
	}

	private String getDataAndWriteACL(HttpServletRequest req, CacheService service, String id) {
		String node = dataBaseServiceHelper(req, service);
		String dataGson = service.getCacheValue(node, id);
		writeACLLog(req, null, dataGson, node);
		LOG.info("Get data:" + dataGson + " from node: " + node);
		return dataGson;
	}

	private String putDataAndWriteACL(HttpServletRequest req, CacheService service, String id, String dataJson) {
		String node = dataBaseServiceHelper(req, service);
		String oldData = service.putValue(node, id, dataJson);
		writeACLLog(req, oldData, dataJson, node);
		LOG.info("Put data:" + dataJson + " in node: " + node);
		return oldData;
	}

}
