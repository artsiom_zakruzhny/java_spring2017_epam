package by.training.epamlab.tags;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.catalina.core.ApplicationServletRegistration;

public class ListURLTag extends TagSupport {
	private static final long serialVersionUID = 1L;
	private Map<String, ApplicationServletRegistration> map;
	private Iterator<Map.Entry<String, ApplicationServletRegistration>> iterator;

	@SuppressWarnings("unchecked")
	@Override
	public int doStartTag() throws JspException {
		map = (Map<String, ApplicationServletRegistration>) pageContext.getServletContext().getServletRegistrations();
		iterator = map.entrySet().iterator();
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doAfterBody() {
		while (iterator.hasNext()) {
			Entry<String, ApplicationServletRegistration> entry = iterator.next();
			pageContext.getRequest().setAttribute("url", entry.getValue().getMappings());
			pageContext.getRequest().setAttribute("servlet", entry.getKey());
			return EVAL_BODY_AGAIN;
		}
		return SKIP_BODY;
	}

}
