package by.training.epamlab.tags;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ResolveURLTag extends SimpleTagSupport {
	private String url;

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void doTag() throws JspException, IOException {
		String resource = null;
		StringWriter sw = new StringWriter();
		PageContext pageContext = (PageContext) getJspContext();
		// getJspContext().setAttribute("url-to-test", "/rest/*");

		Map<String, ? extends ServletRegistration> map = pageContext.getServletContext().getServletRegistrations();
		for (Map.Entry<String, ? extends ServletRegistration> entry : map.entrySet()) {
			if (entry.getValue().getMappings().contains(url)) {
				resource = entry.getKey();
			}
		}

		if (url.charAt(url.length() - 1) == '*') {
			url = url.substring(0, url.length() - 2);
		}
		if (url.charAt(0) == '*') {
			url = "/" + url.substring(2);
		}

		String realPath = pageContext.getServletContext().getRealPath(url);

		getJspContext().setAttribute("url", realPath);
		getJspContext().setAttribute("resource", resource);
		getJspBody().invoke(sw);
		getJspContext().getOut().println("<br/>" + sw.toString());
	}
}
