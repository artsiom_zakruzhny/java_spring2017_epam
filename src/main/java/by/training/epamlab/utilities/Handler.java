package by.training.epamlab.utilities;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Handler implements InvocationHandler {
	private Object object;

	public Handler(Object object) {
		this.object = object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("invoke : " + method.getName());
		return method.invoke(object, args);
	}

}
