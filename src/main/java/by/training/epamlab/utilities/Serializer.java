package by.training.epamlab.utilities;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import by.training.epamlab.annotations.Equal;
import by.training.epamlab.annotations.Serializable;

public class Serializer {
	private static final Logger log = Logger.getLogger(Serializer.class);
	private volatile List<Object> temproraryStorage = new ArrayList<>();

	public Map<String, Object> serialized(Object object) throws IllegalArgumentException, IllegalAccessException,
			NoSuchMethodException, SecurityException, InvocationTargetException {
		log.info("Start serialization.");
		temproraryStorage.add(object);
		Map<String, Object> serialisedStorage = new HashMap<String, Object>();
		Class<?> clazz = object.getClass();
		if (clazz.isAnnotationPresent(Serializable.class)) {
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				if (field.isAnnotationPresent(Serializable.class)) {
					String fieldAlias = field.getAnnotation(Serializable.class).alias();
					String nameMethodGet = "get" + field.getName().substring(0, 1).toUpperCase()
							+ field.getName().substring(1);
					Method getMethod = clazz.getMethod(nameMethodGet);
					if (!field.getType().equals(String.class) && !(field.getType().isPrimitive())) {
						if (!temproraryStorage.contains(field)) {
							serialisedStorage.put(fieldAlias, serialized(field).get(field.getName()));
						}
					}
					if ("".equals(fieldAlias)) {
						serialisedStorage.put(field.getName(), getMethod.invoke(object));
					} else {
						serialisedStorage.put(fieldAlias, getMethod.invoke(object));
					}
				}
			}
		}
		temproraryStorage.clear();
		log.info("Finish serialization.");
		return serialisedStorage;
	}

	public Object deserialized(Map<String, Object> serialisedStorage, Class<?> clazz)
			throws InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException,
			IllegalArgumentException, InvocationTargetException {
		log.info("Start deserialization.");
		if (serialisedStorage.isEmpty()) {
			return null;
		}
		Object object = clazz.newInstance();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Serializable.class)) {
				String fieldAlias = field.getAnnotation(Serializable.class).alias();
				String nameMethodSet = "set" + field.getName().substring(0, 1).toUpperCase()
						+ field.getName().substring(1);
				Method setMethod = clazz.getMethod(nameMethodSet, field.getType());
				if (!"".equals(fieldAlias)) {
					setMethod.invoke(object, serialisedStorage.get(fieldAlias));
				} else {
					setMethod.invoke(object, serialisedStorage.get(field.getName()));
				}
			}
		}
		log.info("Finish deserialization.");
		return object;
	}

	public boolean equalObjects(Object object1, Object object2)
			throws IllegalArgumentException, IllegalAccessException {
		boolean equalsObjects = false;
		Class<? extends Object> clazz1 = object1.getClass();
		Class<? extends Object> clazz2 = object2.getClass();
		if (clazz1 == clazz2) {
			equalsObjects = true;
			Field[] fieldsForObj1 = clazz1.getDeclaredFields();
			Field[] fieldsForObj2 = clazz2.getDeclaredFields();
			for (Field fieldForObj1 : fieldsForObj1) {
				fieldForObj1.setAccessible(true);
				for (Field fieldForObj2 : fieldsForObj2) {
					fieldForObj2.setAccessible(true);
					if (fieldForObj1.equals(fieldForObj2) && fieldForObj1.isAnnotationPresent(Equal.class)) {
						if (!fieldForObj1.get(object1).equals(fieldForObj2.get(object2))) {
							equalsObjects = false;
						}
					}
				}
			}
		}
		return equalsObjects;
	}
}
