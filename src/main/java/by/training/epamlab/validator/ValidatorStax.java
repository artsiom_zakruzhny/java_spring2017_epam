package by.training.epamlab.validator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class ValidatorStax extends Task {
	private boolean checkdep;
	private boolean checkdefault;
	private boolean checknames;
	private List<BuildFile> buildFiles = new ArrayList<BuildFile>();

	private final static String VALUE_DEFAULT_ATTR = "main";
	private final static String ATTRIBUTE_DEFAULT = "default";
	private final static String ATTRIBUTE_NAME = "name";
	private final static String ATTRIBUTE_DEPENDS = "depends";
	private final static String TAG_PROGECT = "project";
	private final static String TAG_TARGET = "target";

	@Override
	public void execute() throws BuildException {
		System.out.println("Start");
		Iterator<BuildFile> iterator = buildFiles.iterator();
		while (iterator.hasNext()) {
			String pathToFile = iterator.next().location;
			InputStream in = null;
			try {
				in = new FileInputStream(pathToFile);
			} catch (FileNotFoundException e) {
				throw new IllegalArgumentException();
			}
			parse(in);
			System.out.println("End.");
		}
	}

	private boolean parse(InputStream in) {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		try {
			XMLStreamReader reader = inputFactory.createXMLStreamReader(in);
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					if (checkdefault) {
						checkDefaultAttribute(reader);
					}
					if (checknames) {
						checkCorrectNames(reader);
					}
					if (checkdep) {
						checkDependents(reader, VALUE_DEFAULT_ATTR);
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new IllegalArgumentException();
		}
		return true;
	}

	public boolean parseWrapper(InputStream in) throws XMLStreamException {
		return parse(in);
	}

	private boolean checkDefaultAttribute(XMLStreamReader reader) {
		boolean defaultAttribute = false;
		if (reader.getLocalName().equals(TAG_PROGECT)) {
			if (reader.getAttributeValue(null, ATTRIBUTE_DEFAULT) == null) {
				System.out.println("project doesn't contain default attribute.");
			} else {
				defaultAttribute = true;
			}
		}
		return defaultAttribute;
	}

	public boolean checkDefaultAttributeWrapper(XMLStreamReader reader) {
		return checkDefaultAttribute(reader);
	}

	private boolean checkCorrectNames(XMLStreamReader reader) {
		boolean correctName = false;
		final String REGEX = "[a-zA-Z-]+";
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			if (reader.getAttributeLocalName(i).equals(ATTRIBUTE_NAME)) {
				if (!reader.getAttributeValue(i).matches(REGEX)) {
					System.out.println("Not correct name:" + reader.getAttributeValue(i));
				} else {
					correctName = true;
				}
			} else {
				return false;
			}
		}
		return correctName;
	}

	public boolean checkCorrectNamesWrapper(XMLStreamReader reader) {
		return checkCorrectNames(reader);
	}

	private boolean checkDependents(XMLStreamReader reader, String defaultTarget) {
		boolean existDependents = false;
		if (reader.getLocalName().equals(TAG_TARGET)) {
			if (reader.getAttributeValue(null, ATTRIBUTE_NAME).equals(defaultTarget)) {
				if (reader.getAttributeValue(null, ATTRIBUTE_DEPENDS) == null) {
					System.out.println("main doesn't contain dependencies.");
				} else {
					existDependents = true;
				}
			}
		}
		return existDependents;
	}

	public boolean checkDependentsWrapper(XMLStreamReader reader) {
		return checkDependents(reader, VALUE_DEFAULT_ATTR);
	}

	public void setCheckdepends(boolean checkdep) {
		this.checkdep = checkdep;
	}

	public void setCheckdefault(boolean checkdefault) {
		this.checkdefault = checkdefault;
	}

	public void setChecknames(boolean checknames) {
		this.checknames = checknames;
	}

	public boolean isCheckdep() {
		return checkdep;
	}

	public boolean isCheckdefault() {
		return checkdefault;
	}

	public boolean isChecknames() {
		return checknames;
	}

	public static class BuildFile {
		private String location;

		public void setLocation(String location) {
			this.location = location;
		}

		public String getLocation() {
			return this.location;
		}

	}

	public List<BuildFile> getBuildFiles() {
		return buildFiles;
	}

	public BuildFile createBuildfile() {
		BuildFile buildFile = new BuildFile();
		buildFiles.add(buildFile);
		return buildFile;
	}
}
