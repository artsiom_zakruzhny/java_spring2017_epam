<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="utils" uri="customtags"%>
<html>
<body>
	<h2>Hello World!</h2>
	<utils:listmapping>
		${url} - ${servlet}
	</utils:listmapping>
	<utils:resolveurl url="/rest/*">
		${url} - ${resource} 
	</utils:resolveurl>
</body>
</html>
