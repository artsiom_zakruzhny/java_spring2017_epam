package annotation;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import by.training.epamlab.annotations.Proxy;
import by.training.epamlab.factorys.ProxyFactory;

public class TestProxy {

	@Proxy(invocationHandler = "by.training.epamlab.utilities.Handler")
	public static class ClassWithProxy {
	}

	public static class ClassWithoutProxy {
	}

	@Proxy(invocationHandler = " ")
	public static class ClassWithNullHandler {
	}

	@Test
	public void testGetInstanceOfWithoutProxy() throws InstantiationException, IllegalAccessException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		assertFalse(ProxyFactory.getInstanceOf(ClassWithoutProxy.class) instanceof java.lang.reflect.Proxy);
	}

	@Test
	public void testGetInstanceOfWithProxy() throws InstantiationException, IllegalAccessException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		assertTrue(ProxyFactory.getInstanceOf(ClassWithProxy.class) instanceof java.lang.reflect.Proxy);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetInstanceOfWithException() throws InstantiationException, IllegalAccessException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		ProxyFactory.getInstanceOf(ClassWithNullHandler.class);
	}

}
