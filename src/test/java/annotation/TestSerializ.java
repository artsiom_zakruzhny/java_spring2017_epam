package annotation;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import by.training.epamlab.annotations.Equal;
import by.training.epamlab.annotations.Serializable;
import by.training.epamlab.utilities.Serializer;

public class TestSerializ {

	@Serializable
	public static class ClassForTestsFieldsWithoutAnnotation {
		private String name;
		private int age;
		private int course;

	}

	@Serializable
	public static class ClassForTestsFieldsWithAnnotation {
		@Equal
		@Serializable(alias = "alias")
		private String name;
		@Serializable
		private int age;
		@Serializable(alias = "obj")
		private Object obj;

		public ClassForTestsFieldsWithAnnotation() {

		}

		public ClassForTestsFieldsWithAnnotation(String name, int age) {
			super();
			this.name = name;
			this.age = age;
		}

		public String getName() {
			return name;
		}

		public Object getObj() {
			return obj;
		}

		public void setObj(Object obj) {
			this.obj = obj;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + age;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ClassForTestsFieldsWithAnnotation other = (ClassForTestsFieldsWithAnnotation) obj;
			if (age != other.age)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

	}

	static class ClassWithoutAnnotation {
		private String name;
		private int age;
	}

	@Test
	public void testSerializedWhenFieldsWithoutAnnotation() throws IllegalArgumentException, IllegalAccessException,
			NoSuchMethodException, SecurityException, InvocationTargetException {
		Serializer serializer = new Serializer();
		ClassForTestsFieldsWithoutAnnotation fieldsWithoutAnnotation = new ClassForTestsFieldsWithoutAnnotation();
		Map<String, Object> map = new HashMap<String, Object>();
		assertEquals(map, serializer.serialized(fieldsWithoutAnnotation));
	}

	@Test
	public void testSerializedWhenFieldHasAnnotation() throws IllegalArgumentException, IllegalAccessException,
			NoSuchMethodException, SecurityException, InvocationTargetException {
		Serializer serializer = new Serializer();
		Map<String, Object> map = new HashMap<String, Object>();
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation = new ClassForTestsFieldsWithAnnotation();
		map.put("alias", null); // test write alias
		map.put("age", 0); // test without alias
		map.put("obj", null); // test when field type is Object
		assertEquals(map, serializer.serialized(fieldsWithAnnotation));
	}

	@Test
	public void testSerializedWhenClassWithoutAnnotation() throws IllegalArgumentException, IllegalAccessException,
			NoSuchMethodException, SecurityException, InvocationTargetException {
		Serializer serializer = new Serializer();
		ClassWithoutAnnotation classWithoutAnnotation = new ClassWithoutAnnotation();
		Map<String, Object> map = new HashMap<String, Object>();
		assertEquals(map, serializer.serialized(classWithoutAnnotation));
	}

	@Test
	public void testEqualObjectsWhenOnceClassWithoutAnnotation()
			throws IllegalArgumentException, IllegalAccessException {
		Serializer serializer = new Serializer();
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation = new ClassForTestsFieldsWithAnnotation("name1", 20);
		ClassForTestsFieldsWithoutAnnotation fieldsWithoutAnnotation = new ClassForTestsFieldsWithoutAnnotation();
		assertFalse(serializer.equalObjects(fieldsWithAnnotation, fieldsWithoutAnnotation));
	}

	@Test
	public void testEqualObjectsWhenObjectsNotEqual() throws IllegalArgumentException, IllegalAccessException {
		Serializer serializer = new Serializer();
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation = new ClassForTestsFieldsWithAnnotation("name1", 20);
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation2 = new ClassForTestsFieldsWithAnnotation("name2", 30);
		assertFalse(serializer.equalObjects(fieldsWithAnnotation, fieldsWithAnnotation2));
	}

	@Test
	public void testEqualObjectsWhenObjectsEqual() throws IllegalArgumentException, IllegalAccessException {
		Serializer serializer = new Serializer();
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation = new ClassForTestsFieldsWithAnnotation("name1", 20);
		ClassForTestsFieldsWithAnnotation fieldsWithAnnotation3 = new ClassForTestsFieldsWithAnnotation("name1", 30);
		assertTrue(serializer.equalObjects(fieldsWithAnnotation, fieldsWithAnnotation3));
	}

	@Test
	public void testDeserialized() throws InstantiationException, IllegalAccessException, NoSuchMethodException,
			SecurityException, IllegalArgumentException, InvocationTargetException {
		Serializer serializer = new Serializer();
		Map<String, Object> testSerializedObject = new HashMap<String, Object>();
		testSerializedObject.put("alias", null);
		testSerializedObject.put("age", 0);
		Object result = serializer.deserialized(testSerializedObject, ClassForTestsFieldsWithAnnotation.class);
		Object expected = new ClassForTestsFieldsWithAnnotation(null, 0);
		assertEquals(expected, result);
	}

	@Test
	public void testDeserializedWhenMapEmpty() throws InstantiationException, IllegalAccessException,
			NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		Serializer serializer = new Serializer();
		Map<String, Object> testSerializedObject = new HashMap<String, Object>();
		Object result = serializer.deserialized(testSerializedObject, ClassForTestsFieldsWithAnnotation.class);
		assertNull(result);
	}

}
