package caches;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import by.training.epamlab.caches.LFU;

public class LFUTest {
	private LFU<Integer, String> lfu = new LFU<Integer, String>(2);

	@Before
	public void setCaches() {
		lfu.put(1, "first");
		lfu.put(2, "second");
		lfu.put(3, "third");
	}

	@Test
	public void testGetLFUWhenExistKey() {
		String existingKey = lfu.get(2);
		assertEquals("second", existingKey);
	}

	@Test
	public void testGetLFUWhenNotExistKey() {
		String notExistingKey = lfu.get(123);
		assertNull(notExistingKey);
	}

	@Test
	public void testNotValidCapacity() {
		try {
			lfu = new LFU<Integer, String>(-1);
		} catch (IllegalArgumentException e) {
			System.out.println("catch exception");
		}
	}

	@Test
	public void testEvictionWhenPut() {
		String keyThatEviction = lfu.get(1); // when was eviction
		assertNull(keyThatEviction);
	}

	@Test
	public void testPutNewValue() {
		String newKeyAndValue = lfu.put(4, "������2");
		assertNull(newKeyAndValue); // must return null, because the value of
									// this key first
	}

	@Test
	public void testPutNewValueWhenHasGivenId() {
		lfu.put(4, "������2");
		String oldValueOfOldKey = lfu.put(4, "������3");
		assertEquals("������2", oldValueOfOldKey);
	}

	@Test
	public void testPutLFU() {
		lfu.put(5, "�����");
		String newResult = lfu.get(5);
		assertEquals("�����", newResult);
	}

	@Test
	public void testToStringLFU() {
		String resultStr = lfu.toString();
		assertNotNull(resultStr);
		assertEquals("{2=second; 1, 3=third; 1}", resultStr);
	}

	@Test
	public void testDeleteFromWhenExistKey() {
		lfu.put(1, "first");
		String result = lfu.delete(1);
		assertEquals("first", result);
	}

	@Test
	public void testDeleteFromWhenNotExistKey() {
		assertNull(lfu.delete(123));
	}

}
