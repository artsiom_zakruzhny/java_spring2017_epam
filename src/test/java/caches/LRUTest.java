package caches;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import by.training.epamlab.caches.LRU;

public class LRUTest {
	private LRU<Integer, String> lru = new LRU<Integer, String>(2);

	@Before
	public void setCaches() {
		lru.put(1, "101");
		lru.put(2, "102");
		lru.put(3, "103");
	}

	@Test
	public void testGetExistKey() {
		String existingKey = lru.get(2);
		assertEquals("102", existingKey);
	}

	@Test
	public void testGetNotExistKey() {
		String notExistingKey = lru.get(1);
		assertNull(notExistingKey);
	}

	@Test
	public void testPutNewValue() {
		String newKeyAndValue = lru.put(4, "104");
		assertNull(newKeyAndValue); // must return null, because the value of
									// this key first
	}

	@Test
	public void testPut() {
		lru.put(123, "104");
		String newKey = lru.get(123);
		assertEquals("104", newKey);
	}

	@Test
	public void testPutNewValueWhenHasGivenId() {
		lru.put(3, "103");
		String oldResultIfKeyWas = lru.put(3, "103_2.0");
		assertEquals("103", oldResultIfKeyWas); // must return old value for
												// used key
	}

	@Test
	public void testPutEvection() {
		lru = new LRU<Integer, String>(2);
		lru.put(1, "101");
		lru.put(2, "102");
		lru.put(3, "103");
		String keyThatEviction = lru.get(1);
		assertNull(keyThatEviction);
	}

	@Test
	public void testToStringLRU() {
		String resultStr = lru.toString();
		assertNotNull(resultStr);
		assertEquals("{2=102, 3=103}", resultStr);
	}

	@Test
	public void testDeleteWhenExistKey() {
		lru.put(1, "first");
		String result = lru.delete(1);
		assertEquals("first", result);
	}

	@Test
	public void testDeleteWhenNotExistKey() {
		assertNull(lru.delete(321));
	}

	@Test
	public void testNotValidCapacity() {
		try {
			lru = new LRU<Integer, String>(-1);
		} catch (IllegalArgumentException e) {
			System.out.println("catch exception");
		}
	}
}
