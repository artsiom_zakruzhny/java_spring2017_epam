package services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

import java.sql.SQLException;

import org.junit.Test;

import by.training.epamlab.caches.LFU;
import by.training.epamlab.caches.LRU;
import by.training.epamlab.services.CacheService;
import by.training.epamlab.services.DataBaseService;

public class TestDataService {

	@Test
	public void testServicePutLFUCacheNewValue() throws SQLException {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.insertData("node1", "id1", "{\"name\":\"Art\"}")).thenReturn("{\"name\":\"Art\"}");
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "id1", "{\"name\":\"Art\"}");
		assertEquals("{\"name\":\"Art\"}", service.getCacheValue("node1", "id1"));
	}

	@Test
	public void testServicePutLRUCacheNewValue() {
		CacheService service = new CacheService();
		CacheService.setCache(1, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.insertData("node1", "second", "{\"name\":\"Dima\"}")).thenReturn("{\"name\":\"Dima\"}");
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "second", "{\"name\":\"Dima\"}");
		assertEquals("{\"name\":\"Dima\"}", service.getCacheValue("node1", "second"));
	}

	@Test
	public void testServicePutWhenGivenIdExist() {
		CacheService service = new CacheService();
		CacheService.setCache(1, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.insertData("node1", "second", "{\"name\":\"Dima\"}")).thenReturn("{\"name\":\"Dima\"}");
		when(dataBaseService.insertData("node1", "second", "{\"name\":\"Artem\"}")).thenReturn("{\"name\":\"Artem\"}");
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "second", "{\"name\":\"Artem\"}");
		String result = service.putValue("node1", "second", "{\"name\":\"Dima\"}");
		assertEquals("{\"name\":\"Artem\"}", result);
	}

	@Test
	public void testServiceGetCacheValueValidateStrLRU() {
		CacheService service = new CacheService();
		CacheService.setCache(1, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.selectById("node1", "second")).thenReturn(null);
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "second", "{\"name\":\"Artem2\"}");
		assertEquals("{\"name\":\"Artem2\"}", service.getCacheValue("node1", "second"));
	}

	@Test
	public void testServiceGetValueWhenNotExistInLRUCache() {
		CacheService service = new CacheService();
		CacheService.setCache(1, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.selectById("node1", "id4")).thenReturn(null);
		service.setDataBaseService(dataBaseService);
		assertNull(service.getCacheValue("node1", "id4"));
	}

	@Test
	public void testServiceGetCacheValueValidateStrLFU() {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.selectById("node1", "second")).thenReturn(null);
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "second", "{\"name\":\"Artem2\"}");
		assertEquals("{\"name\":\"Artem2\"}", service.getCacheValue("node1", "second"));
	}

	@Test
	public void testServiceGetValueWhenNotExistInLFUCache() {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.selectById("node1", "id4")).thenReturn(null);
		service.setDataBaseService(dataBaseService);
		assertNull(service.getCacheValue("node1", "id4"));
	}

	@Test
	public void testServiceDeleteDataLFUCacche() {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.deleteData("node1", "id1")).thenReturn("{\"name\":\"Art\"}");
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "id1", "{\"name\":\"Art\"}");
		assertEquals("{\"name\":\"Art\"}", service.deleteValue("node1", "id1"));
	}

	@Test
	public void testServiceDeleteDataLRUCacche() {
		CacheService service = new CacheService();
		CacheService.setCache(1, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.deleteData("node1", "id1")).thenReturn("{\"name\":\"Art\"}");
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "id1", "{\"name\":\"Art\"}");
		assertEquals("{\"name\":\"Art\"}", service.deleteValue("node1", "id1"));
	}

	@Test
	public void testServiceDeleteDataWhenNotData() {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.deleteData("node1", "id1")).thenReturn(null);
		service.setDataBaseService(dataBaseService);
		service.putValue("node1", "id1", "{\"name\":\"Art\"}");
		assertNull(service.deleteValue("node1", "id1"));
	}

	@Test
	public void testServiceDeleteDataWhenNotDatainCache() {
		CacheService service = new CacheService();
		CacheService.setCache(0, 3);
		DataBaseService dataBaseService = mock(DataBaseService.class);
		when(dataBaseService.deleteData("node1", "id1")).thenReturn("{\"name\":\"Art\"}");
		service.setDataBaseService(dataBaseService);
		assertEquals("{\"name\":\"Art\"}", service.deleteValue("node1", "id1"));
	}

	@Test
	public void testSetLFUCache() {
		CacheService.setCache(0, 3);
		boolean flag = CacheService.getCache() instanceof LFU;
		assertEquals(true, flag);
	}

	@Test
	public void testSetLRUCache() {
		CacheService.setCache(1, 3);
		boolean flag = CacheService.getCache() instanceof LRU;
		assertEquals(true, flag);
	}

	@Test
	public void testServiceSetCacheWhenUndefinedAlgorithm() {
		try {
			CacheService.setCache(123, 3);
		} catch (IllegalArgumentException e) {
			System.out.println("catch exception if not valid algorithm");
		}
	}
}
