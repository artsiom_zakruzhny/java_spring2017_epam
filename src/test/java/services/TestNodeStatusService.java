package services;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import by.training.epamlab.services.NodeStatusService;

public class TestNodeStatusService {

	@Test
	public void testGetSingleNodeStatusIfExist() throws SQLException {
		NodeStatusService nodeStatusService = new NodeStatusService();
		Map<String, String> statusNodes = new HashMap<>();
		statusNodes.put("node1", "OK");
		statusNodes.put("node0", "OK");
		NodeStatusService.setStatusNodesWithReplics(statusNodes);
		String result = nodeStatusService.getNodeStatus("/node0");
		assertEquals("OK", result);
	}

	@Test
	public void testGetSingleNodeStatusIfNotExist() throws SQLException {
		NodeStatusService nodeStatusService = new NodeStatusService();
		String result = nodeStatusService.getNodeStatus("lol");
		assertNull(result);
	}

	@Test
	public void testGetClusterStatus() throws SQLException {
		NodeStatusService nodeStatusService = new NodeStatusService();
		Map<String, String> statusNodes = new HashMap<>();
		statusNodes.put("node1", "OK");
		NodeStatusService.setStatusNodesWithReplics(statusNodes);
		String result = nodeStatusService.getNodeStatus(null);
		assertEquals("{node1=OK}", result);
	}

	@Test
	public void testGetStatusIfNotInfo() throws SQLException {
		NodeStatusService nodeStatusService = new NodeStatusService();
		NodeStatusService.setStatusNodesWithReplics(null);
		String result = nodeStatusService.getNodeStatus(null);
		assertEquals("Information is not collected", result);
	}

	@Test
	public void testCheckClusterStatusWhenOneNodeFail() throws SQLException {
		Map<String, String> map = new HashMap<>();
		map.put("node0", "OK");
		map.put("node1", "crach");
		NodeStatusService.setStatusNodes(map);
		boolean statusCluster = NodeStatusService.checkClusterStatus();
		assertEquals(false, statusCluster);
	}

	@Test
	public void testCheckClusterStatusWhenClusterWork() throws SQLException {
		Map<String, String> map = new HashMap<>();
		map.put("node0", "OK");
		map.put("node1", "OK");
		NodeStatusService.setStatusNodes(map);
		boolean statusCluster = NodeStatusService.checkClusterStatus();
		assertEquals(true, statusCluster);
	}

}
