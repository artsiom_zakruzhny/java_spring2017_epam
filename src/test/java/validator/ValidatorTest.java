package validator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.Test;

import by.training.epamlab.validator.ValidatorStax;

public class ValidatorTest {
	@Test
	public void testCheckDefaultAttribute() {
		ValidatorStax validator = new ValidatorStax();
		XMLStreamReader reader = mock(XMLStreamReader.class);

		when(reader.getLocalName()).thenReturn("not-project");
		when(reader.getAttributeValue(null, "default")).thenReturn(null);
		boolean resultWhenProjectIsNull = validator.checkDefaultAttributeWrapper(reader);
		assertEquals(false, resultWhenProjectIsNull);

		when(reader.getLocalName()).thenReturn("project");
		when(reader.getAttributeValue(null, "default")).thenReturn(null);
		boolean resultWhenDefaultIsNull = validator.checkDefaultAttributeWrapper(reader);
		assertEquals(false, resultWhenDefaultIsNull);

		when(reader.getAttributeValue(null, "default")).thenReturn("some_value");
		boolean resultWhenDefaultHasValue = validator.checkDefaultAttributeWrapper(reader);
		assertEquals(true, resultWhenDefaultHasValue);
	}

	@Test
	public void testCheckDependents() {
		ValidatorStax validator = new ValidatorStax();
		XMLStreamReader reader = mock(XMLStreamReader.class);

		when(reader.getLocalName()).thenReturn("target");
		when(reader.getAttributeValue(null, "name")).thenReturn("main");
		when(reader.getAttributeValue(null, "depends")).thenReturn(null);
		boolean resultWhenDependsIsNull = validator.checkDependentsWrapper(reader);
		assertEquals(false, resultWhenDependsIsNull);

		when(reader.getAttributeValue(null, "name")).thenReturn("main");
		when(reader.getAttributeValue(null, "depends")).thenReturn("some target");
		boolean resultWhenDefaultIsNotNull = validator.checkDependentsWrapper(reader);
		assertEquals(true, resultWhenDefaultIsNotNull);

		when(reader.getAttributeValue(null, "name")).thenReturn("Notmain");
		boolean resultWhenNotMainTarget = validator.checkDependentsWrapper(reader);
		assertEquals(false, resultWhenNotMainTarget);

		when(reader.getLocalName()).thenReturn("NotTarget");
		boolean resultWhenHasNotTargetTag = validator.checkDependentsWrapper(reader);
		assertEquals(false, resultWhenHasNotTargetTag);

	}

	@Test
	public void testCorrectNames() {
		ValidatorStax validator = new ValidatorStax();
		XMLStreamReader reader = mock(XMLStreamReader.class);

		when(reader.getAttributeCount()).thenReturn(1);
		when(reader.getAttributeLocalName(0)).thenReturn("name");
		when(reader.getAttributeValue(0)).thenReturn("correct-name");
		boolean resultCorrectName = validator.checkCorrectNamesWrapper(reader);
		assertEquals(true, resultCorrectName);

		when(reader.getAttributeLocalName(0)).thenReturn("not-name");
		boolean resultHasnotName = validator.checkCorrectNamesWrapper(reader);
		assertEquals(false, resultHasnotName);

		when(reader.getAttributeLocalName(0)).thenReturn("name");
		when(reader.getAttributeValue(0)).thenReturn("123NotCorrect-name");
		boolean resultNotCorrectNameWithNumber = validator.checkCorrectNamesWrapper(reader);
		assertEquals(false, resultNotCorrectNameWithNumber);

		when(reader.getAttributeValue(0)).thenReturn("correct-name?");
		boolean resultNotCorrectNameWithSmb = validator.checkCorrectNamesWrapper(reader);
		assertEquals(false, resultNotCorrectNameWithSmb);
	}

	@Test
	public void testParseStaxWithException() throws XMLStreamException {
		ValidatorStax validator = new ValidatorStax();
		InputStream in = mock(FileInputStream.class);
		try {
			validator.parseWrapper(in); // in == null and catch
										// xmlStreamException
		} catch (IllegalArgumentException xmlException) {
			System.out.println("catch xmlStreamException");
		}
	}
}
